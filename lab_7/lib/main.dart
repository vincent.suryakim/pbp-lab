import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Tambahkan Data Provinsi',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Tambahkan Data Provinsi'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String _status = "";

  void _updateStatus(String status) {
    setState(() {
      _status = status;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: FractionallySizedBox(
          widthFactor: 0.9,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TextFormField(
                onChanged: (String value) {
                  _updateStatus("");
                },
                keyboardType: TextInputType.text,
                decoration: const InputDecoration(
                  labelText: 'Provinsi',
                  border: OutlineInputBorder(),
                  prefixIcon: Icon(Icons.public),
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              TextFormField(
                onChanged: (String value) {
                  _updateStatus("");
                },
                keyboardType: TextInputType.number,
                decoration: const InputDecoration(
                  labelText: 'Terkonfirmasi',
                  border: OutlineInputBorder(),
                  prefixIcon: Icon(Icons.edit_rounded),
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              TextFormField(
                onChanged: (String value) {
                  _updateStatus("");
                },
                keyboardType: TextInputType.number,
                decoration: const InputDecoration(
                  labelText: 'Kasus Aktif',
                  border: OutlineInputBorder(),
                  prefixIcon: Icon(Icons.edit_rounded),
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              TextFormField(
                onChanged: (String value) {
                  _updateStatus("");
                },
                keyboardType: TextInputType.number,
                decoration: const InputDecoration(
                  labelText: 'Sembuh',
                  border: OutlineInputBorder(),
                  prefixIcon: Icon(Icons.edit_rounded),
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              TextFormField(
                onChanged: (String value) {
                  _updateStatus("");
                },
                keyboardType: TextInputType.number,
                decoration: const InputDecoration(
                  labelText: 'Meninggal',
                  border: OutlineInputBorder(),
                  prefixIcon: Icon(Icons.edit_rounded),
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              Container(
                clipBehavior: Clip.antiAliasWithSaveLayer,
                width: double.infinity,
                height: 60,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                ),
                child: MaterialButton(
                  onPressed: () => _updateStatus("Data telah ditambahkan!"),
                  color: Colors.blue,
                  child: const Text(
                    'Tambahkan',
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Text(
                _status,
                style: Theme.of(context).textTheme.subtitle2,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
