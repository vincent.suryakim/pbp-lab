from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

from lab_1.models import Friend
from .forms import FriendForm

# Create your views here.

mhs_name = 'Vincent Suryakim'
lab = 'Lab3'

@login_required(login_url='/admin/login/?next=/lab-3')
def index(request):
    response = {'name': mhs_name,
                'lab': lab,
                'friends': Friend.objects.all()}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/?next=/lab-3/add')
def add_friend(request):
    context = {}
    response = {'name': mhs_name,
                'lab': lab}

    form = FriendForm(request.POST or None)

    if (form.is_valid() and request.method == "POST"):
        form.save()
        return HttpResponseRedirect("/lab-3")
    
    context['form'] = form

    return render(request, "lab3_form.html", response)