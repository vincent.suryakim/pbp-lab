import 'package:flutter/material.dart';

import '../widgets/global_info.dart';

class SebaranScreen extends StatelessWidget {
  const SebaranScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: SizedBox(
      width: double.infinity,
      child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
        const SizedBox(
          height: 8,
        ),
        const GlobalInfoWidget(
            judul: "Terkonfirmasi",
            jumlah: "2.690.515",
            background: "assets/images/covid.jpeg"),
        const GlobalInfoWidget(
            judul: "Kasus Aktif",
            jumlah: "5.335",
            background: "assets/images/aktif.png"),
        const GlobalInfoWidget(
            judul: "Sembuh",
            jumlah: "2.592.314",
            background: "assets/images/sembuh.png"),
        const GlobalInfoWidget(
            judul: "Meninggal",
            jumlah: "92.866",
            background: "assets/images/meninggal.jpg"),
        const SizedBox(
          height: 40,
        ),
        const Text(
          "Persebaran per-Provinsi",
          style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
        ),
        const SizedBox(
          height: 10,
        ),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: DataTable(
            columns: const [
              DataColumn(label: Text("#")),
              DataColumn(label: Text("Provinsi")),
              DataColumn(label: Text("Terkonfirmasi")),
              DataColumn(label: Text("Kasus Aktif")),
              DataColumn(label: Text("Sembuh")),
              DataColumn(label: Text("Meninggal")),
            ],
            rows: const [
              DataRow(cells: [
                DataCell(Text("1",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ))),
                DataCell(Text("Jawa Tengah")),
                DataCell(Text("484.621")),
                DataCell(Text("1.939")),
                DataCell(Text("452.588")),
                DataCell(Text("30.094")),
              ]),
              DataRow(cells: [
                DataCell(Text("2",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ))),
                DataCell(Text("Jawa Barat")),
                DataCell(Text("705.155")),
                DataCell(Text("1.176")),
                DataCell(Text("689.293")),
                DataCell(Text("14.686")),
              ]),
              DataRow(cells: [
                DataCell(Text("3",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ))),
                DataCell(Text("DKI Jakarta")),
                DataCell(Text("860.962")),
                DataCell(Text("1.065")),
                DataCell(Text("846.320")),
                DataCell(Text("13.577")),
              ]),
              DataRow(cells: [
                DataCell(Text("4",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ))),
                DataCell(Text("Jawa Timur")),
                DataCell(Text("397.857")),
                DataCell(Text("511")),
                DataCell(Text("357.754")),
                DataCell(Text("29.592")),
              ]),
              DataRow(cells: [
                DataCell(Text("5",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ))),
                DataCell(Text("Sulawesi Selatan")),
                DataCell(Text("109.576")),
                DataCell(Text("379")),
                DataCell(Text("109.966")),
                DataCell(Text("2.231")),
              ]),
              DataRow(cells: [
                DataCell(Text("6",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ))),
                DataCell(Text("Banten")),
                DataCell(Text("132.344")),
                DataCell(Text("265")),
                DataCell(Text("129.393")),
                DataCell(Text("2.686")),
              ]),
            ],
          ),
        ),
        const SizedBox(
          height: 20,
        ),
      ]),
    ));
  }
}
