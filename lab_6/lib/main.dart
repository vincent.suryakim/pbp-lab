import 'package:flutter/material.dart';

import 'screens/home_screen.dart';
import 'screens/sebaran_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Infid',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Infid'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Map<String, Object>> _pages = [];
  int _selectedPageIndex = 0;

  void _selectPage(int index) {
    setState(() {
      _selectedPageIndex = index;
    });
  }

  @override
  void initState() {
    _pages = [
      {
        'page': const HomeScreen(),
        'title': 'Infid',
      },
      {
        'page': const SebaranScreen(),
        'title': 'Persebaran Data',
      },
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(_pages[_selectedPageIndex]['title'] as String),
        backgroundColor: const Color.fromRGBO(21, 38, 54, 1),
      ),
      drawer: Drawer(
        // Add a ListView to the drawer. This ensures the user can scroll
        // through the options in the drawer if there isn't enough vertical
        // space to fit everything.
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
            const DrawerHeader(
              child:
                  Text('Infid by A04', style: TextStyle(color: Colors.white)),
              decoration: BoxDecoration(
                  color: Color.fromRGBO(21, 38, 54, 1),
                  image: DecorationImage(
                      image: AssetImage("assets/images/covid.jpeg"),
                      colorFilter: ColorFilter.mode(
                          Color.fromRGBO(0, 0, 0, 0.3), BlendMode.dstATop),
                      fit: BoxFit.cover)),
            ),
            ListTile(
              title: const Text('Home'),
              onTap: () {
                _selectPage(0);
              },
            ),
            ListTile(
              title: const Text('Sebaran'),
              onTap: () {
                _selectPage(1);
              },
            ),
          ],
        ),
      ),
      body: (_pages[_selectedPageIndex]['page']) as Widget,
    );
  }
}
