from django.shortcuts import render
from django.http import HttpResponseRedirect

from lab_2.models import Note

# Create your views here.

def index(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab5_index.html', response)

def edit_note(request):
    if (request.method == "POST"):
        id = request.POST.get("id")
        note_to = request.POST.get("note_to")
        note_from = request.POST.get("note_from")
        title = request.POST.get("title")
        message = request.POST.get("message")

        note = Note.objects.get(id=id)
        note.note_to = note_to
        note.note_from = note_from
        note.title = title
        note.message = message
        note.save()

    return HttpResponseRedirect("/lab-5")

def delete_note(request):
    if (request.method == "POST"):
        id = request.POST.get("id")
        Note.objects.filter(id=id).delete()
    return HttpResponseRedirect("/lab-5")