from django.urls import path
from .views import index, delete_note, edit_note

urlpatterns = [
    path('', index, name='index'),
    path('delete_note', delete_note, name='delete_note'),
    path('edit_note', edit_note, name='edit_note'),
]
