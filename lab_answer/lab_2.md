## 1. Apakah perbedaan antara JSON dan XML?

Secara garis besar, XML adalah sebuah bahasa markup, sementara JSON adalah sebuah format data. Untuk lebih mendetailnya, perbedaan antara XML dan JSON adalah:

| Perbedaan   | XML         | JSON        |
| ----------- | ----------- | ----------- |
| Bahasa      | XML adalah sebuah bahasa markup yang menggunakan tag untu mendefinisikan suatu elemen       | JSON adalah sebuah format yang ditulis dalam JavaScript       |
| Kecepatan   | Lambat dalam penguraian karena ukuran filenya yang besar        | Sangat cepat dalam penguraian karena ukuran filenya yang sangat kecil        |
| Ukuran File | Ukuran file yang besar karena struktur tag yang digunakannya membuatnya besar dan rumit untuk dibaca | Ukuran file yang lebih ringan karena lebih ringkas |
| Keamanan | Rentan terhadap beberapa serangan | Lebih aman kecuali jika JSONP digunakan karena berpotensi menyebabkan serangan CSRF (Cross-Site Request Forgery) |
| Tujuan | Menyimpan dan mengangkut data antar-aplikasi melalui internet | Format pertukaran data ringan yang lebih mudah bagi komputer untuk mengurai data yang dikirim |


## 2. Apakah perbedaan antara HTML dan XML?

Secara garis besar, HTML merupakan sebuah bahasa markup, sementara XML adalah sebuah bahasa markup standar yang mendefinisikan bahasa markup lain. Untuk lebih mendetailnya, perbedaan antara HTML dan XML adalah:

| Perbedaan   | HTML        | XML         |
| ----------- | ----------- | ----------- |
| Bahasa | HTML adalah sebuah bahasa markup | XML adalah sebuah bahasa markup standar yang mendefinisikan bahasa markup lain |
| Fungsi | Menampilkan data | Mentransfer data |
| Case-sensitivity | HTML bukanlah sebuah bahasa yang case-sensitive | XML merupakan bahasa yang case-sensitive |
| Syntax | HTML memiliki tagsnya sendiri | Tags pada XML didefinisikan sesuai dengan kebutuhan programmernya |